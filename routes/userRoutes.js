const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUserController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController)

router.put('/updateUserUsername/:id', userControllers.updateUserUsernameController)

module.exports = router;

