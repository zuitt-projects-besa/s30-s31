const User = require('../models/User');

/*1. Create User*/
// localhost:4000/users
module.exports.createUserController = (req, res) => {
	console.log(req.body);

	User.findOne({username: req.body.username}).then(result => {
		if (result != null && result.username == req.body.username){
			return res.send('Duplicate user found')
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

/*2. Display Users*/
// localhost:4000/users
module.exports.getAllUserController = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

/*3. Get single User*/
// localhost:4000/users/getSingleUser/6243f17e997aba910ddd3580
module.exports.getSingleUserController = (req, res) => {
	User.findById(req.params.id)

	.then(result => res.send(result))
	.catch(result => res.send(error));
};

/*4. Update User Username*/
// localhost:4000/users/updateUserUsername/6243f17e997aba910ddd3580
module.exports.updateUserUsernameController = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		username: req.body.username
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})

	.then(result => res.send(result))
	.catch(result => res.send(error))

};
